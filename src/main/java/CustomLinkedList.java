package main.java;

/**
 * Lista Linked List
 * Created by RENT on 2017-09-04.
 */
public class CustomLinkedList {
    private Node root; //początek listy

    public CustomLinkedList() {
        this.root = null;
    }

    public void add (Object data){
        Node wezel = new Node(data);
        if (root == null){
            root = wezel;
        } else {
            Node tmp = root;
            while(tmp.getNext()!=null){
                tmp = tmp.getNext();
            }
            tmp.setNext(wezel);
        }
    }

    public void print (){
        Node tmp = root;
        if(root==null){
            return;
        }
        while(tmp.getNext()!=null){
            System.out.println(tmp.getData());
            tmp = tmp.getNext();
        }
        System.out.println(tmp.getData());
    }

    public void remove (){
        if(root!=null){
            root = root.getNext();
        }
    }

    public Object get(int index){
        if (root==null){
            return null;
        }
        int counter = 0;
        Node tmp =root;
        while(tmp.getNext()!=null&&counter!=index){
            tmp = tmp.getNext();
            counter++;
        }
        if (counter==index){
            return tmp.getData();
        }else{
            System.err.println("niedostateczna liczba elementów");
            return null;
        }
    }

    public void add(Object data, int index){
        Node wezel = new Node(data);
        int counter = 0;
        Node tmp =root;
        Node tmpPrev =null;
        while(tmp.getNext()!=null&&counter!=index){
            tmpPrev=tmp;
            tmp = tmp.getNext();
            counter++;
        }
        if(tmpPrev==null){
            wezel.setNext(root);
            root = wezel;
        }else if (counter==index){
            wezel.setNext(tmpPrev.getNext());
            tmpPrev.setNext(wezel);
        } else if(tmp.getNext()==null&&(counter+1)==index){
            wezel.setNext(tmp.getNext());
            tmp.setNext(wezel);
        }
        else{
            System.err.println("nie ma takiego elementu o takim indeksie");
            return;
        }
    }

    public int size(){
        if (root==null){
            return 0;
        }
        int counter = 1;
        Node tmp =root;
        while(tmp.getNext()!=null){
            tmp = tmp.getNext();
            counter++;
        }
        return counter;
    }

    public void  removeAtPosition(int index){
        if(index == 0){
            remove();
            return;
        }
        int counter = 0;
        Node tmp =root;
        Node tmpPrev =null;
        while(tmp.getNext()!=null&&counter!=index){
            tmpPrev=tmp;
            tmp = tmp.getNext();
            counter++;
        }
        if (counter==index){
            tmpPrev.setNext(tmp.getNext());
        } else if(tmp.getNext()==null&&(counter+1)==index){
            tmpPrev.setNext(null);
        }
        else{
            System.err.println("nie ma takiego elementu o takim indeksie");
            return;
        }

    }

}
